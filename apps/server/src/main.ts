import { ValidationError, ValidationPipe } from '@nestjs/common';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { PORT } from './app/config';
import { FallbackExceptionFilter } from './app/filters/fallback.filter';
import { HttpExceptionFilter } from './app/filters/http.filter';
import { ValidationException } from './app/filters/validation.exception';
import { ValidationFilter } from './app/filters/validation.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');

  app.enableCors();

  app.use(helmet());

  app.use(
    rateLimit({
      windowMs: 1 * 60 * 1000, // 1 minute
      max: 100, // limit each IP to 100 requests per windowMs
    })
  );

  await app.listen(PORT);

  app.useGlobalFilters(
    new FallbackExceptionFilter(),
    new HttpExceptionFilter(),
    new ValidationFilter()
  );

  app.useGlobalPipes(
    new ValidationPipe({
      skipMissingProperties: true,
      exceptionFactory: (errors: ValidationError[]) => {
        const messages = errors.map(
          (error) => `${error.property} has wrong value ${error.value},
                ${Object.values(error.constraints).join(', ')} `
        );

        return new ValidationException(messages);
      },
    })
  );
}
bootstrap();
