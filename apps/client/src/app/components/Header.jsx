import React from 'react';

const Header = () => {
  return (
    <div className="header-component">
      <div className="container content">
        <div className="item">
          <h2 className="typography text-white">HN Feed</h2>
        </div>
        <div className="item">
          <h5 className="typography text-white">We &lt;3 hacker news!</h5>
        </div>
      </div>
    </div>
  );
};

export default Header;
