import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ScheduleModule} from '@nestjs/schedule';
import {MONGO_CONNECTION} from './config';
import {PostModule} from './modules/posts/post.module';
import {ScheduleService} from './services/schedule.service';

@Module({
	imports: [
		ScheduleModule.forRoot(),
		MongooseModule.forRoot(MONGO_CONNECTION, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useCreateIndex: true,
			useFindAndModify: false,
		}),
		PostModule,
	],
	providers: [ScheduleService],
})
export class AppModule {}
