import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema(
	{
		title: String,
		url: String,
		author: String,
		created_at: Date,
		status: {type: Boolean, default: true},
	},
	{
		toJSON: {
			transform: (_, ret) => {
				ret.id = ret._id;
				delete ret._id;
				delete ret.__v;
			},
		},
	},
);
