import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { ToolsService } from './tools.service';
import { HttpService, HttpModule } from '@nestjs/common';

import { Post } from './../../../dto/post.dto';
import { fetchObject, result } from './test-helper';

describe('ToolsService', () => {
  let toolsService: ToolsService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [ToolsService],
    }).compile();

    toolsService = module.get<ToolsService>(ToolsService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(toolsService).toBeDefined();
  });

  describe('fetchPosts', () => {
    it('fetches successfully data from an API', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementationOnce(() => of(fetchObject));
      await expect(toolsService.fetchPosts()).resolves.toEqual(fetchObject);
    });
  });

  describe('formatPosts', () => {
    it('should return the data when there is no last post', async () => {
      const data = toolsService.formatPosts(fetchObject.data.hits);

      expect(data).toEqual(result.reverse());
    });

    it('should return an empty array when bad params', async () => {
      const badParam: any = 'badParam';
      const data = toolsService.formatPosts(badParam);

      expect(Array.isArray(data)).toBeTruthy();
      expect(data.length).toEqual(0);
    });

    it('should return the correct result when filtered by creation date', async () => {
      const lastPostCreated = new Date('2020-12-27T14:31:54.000Z');
      const data = toolsService.formatPosts(
        fetchObject.data.hits,
        lastPostCreated
      );

      const newResult = [...result];
      newResult.shift();

      expect(data).toEqual(newResult);
    });
  });

  describe('getNewPosts', () => {
    it('should invoke functions fetchPosts and formatPosts', async () => {
      jest.spyOn(toolsService, 'fetchPosts');
      jest.spyOn(toolsService, 'formatPosts');

      await toolsService.getNewPosts();

      expect(toolsService.fetchPosts).toBeCalled();
      expect(toolsService.fetchPosts).toBeCalledTimes(1);
      expect(toolsService.formatPosts).toBeCalled();
      expect(toolsService.formatPosts).toBeCalledTimes(1);
    });

    it('should return an empty array when there is an error in fetchPosts', async () => {
      jest.spyOn(toolsService, 'fetchPosts').mockRejectedValue(new Error());

      const data = await toolsService.getNewPosts();

      expect(Array.isArray(data)).toBeTruthy();
      expect(data.length).toEqual(0);
    });
  });
});
