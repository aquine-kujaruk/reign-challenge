## Reign Challenge

#### Stack

- React.js
- Redux
- Node.js
- Nest.js
- MongoDB
- Docker
- Typescript

### Prerequisites

Make sure you have the below installed on your machine.

- [x] **Docker** : https://docs.docker.com/engine/install/
- [x] **Docker-Compose** : https://docs.docker.com/compose/install/
- [x] **Node** : https://nodejs.org/en/

### Quick start

```bash
# clone repository
git clone https://gitlab.com/aquine-kujaruk/reign-challenge.git

# open directory
cd reign-challenge

# run project
docker build . -t my-base-image:nx-base && docker-compose up
```

The app will be served on `http://localhost:4200`
