import { Post } from './../../../dto/post.dto';

export const fetchObject = {
  data: {
    hits: [
      {
        created_at: '2020-12-27T18:39:24.000Z',
        title: 'title-1',
        story_url: 'http://fake-url-1.com',
        author: 'Author-1',
        story_title: 'story_title-1',
      },
      {
        created_at: '2020-12-27T18:26:12.000Z',
        title: 'title-2',
        story_url: 'http://fake-url-2.com',
        author: 'Author-2',
        story_title: null,
      },
      {
        created_at: '2020-12-27T15:34:12.000Z',
        title: null,
        story_url: 'http://fake-url-3.com',
        author: 'Author-3',
        story_title: 'story_title-3',
      },
      {
        created_at: '2020-12-27T13:31:54.000Z',
        title: 'title-4',
        story_url: null,
        author: 'Author-4',
        story_title: 'story_title-4',
      },
      {
        created_at: '2020-12-27T11:11:07.000Z',
        title: null,
        story_url: 'http://fake-url-5.com',
        author: 'Author-5',
        story_title: null,
      },
    ],
  },
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {},
};

export const result: Partial<Post>[] = [
  {
    created_at: '2020-12-27T18:39:24.000Z',
    url: 'http://fake-url-1.com',
    author: 'Author-1',
    title: 'story_title-1',
  },
  {
    created_at: '2020-12-27T18:26:12.000Z',
    url: 'http://fake-url-2.com',
    author: 'Author-2',
    title: 'title-2',
  },
  {
    created_at: '2020-12-27T15:34:12.000Z',
    url: 'http://fake-url-3.com',
    author: 'Author-3',
    title: 'story_title-3',
  },
  {
    created_at: '2020-12-27T13:31:54.000Z',
    url: null,
    author: 'Author-4',
    title: 'story_title-4',
  },
];
