// PORT
export const PORT = process.env.PORT || 5000;

// Database connection
export const MONGO_CONNECTION =
	process.env.MONGO_CONNECTION || 'mongodb://127.0.0.1:27017/reign';

// Database connection
export const URL_ALGOLIA_API = 'https://hn.algolia.com/api/v1';
