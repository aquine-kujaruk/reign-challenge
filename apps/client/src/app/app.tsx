import React from 'react';
import { Provider } from 'react-redux';
import Header from './components/Header';
import MyTable from './components/MyTable';
import generateStore from './store';

const App = () => {
  const store = generateStore();
  return (
    <Provider store={store}>
      <Header />
      <MyTable />
    </Provider>
  );
};

export default App;
