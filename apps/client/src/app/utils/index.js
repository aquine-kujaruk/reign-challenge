import moment from 'moment';

export const formatDate = (date) => {
  const current = moment(date);
  const today = moment().startOf('day');
  const yesterday = moment().subtract(1, 'days').startOf('day');

  if (current > today) return moment(current).format('hh:mm a');
  else if (current > yesterday) return 'Yesterday';
  else return moment(current).format('MMM DD');
};
