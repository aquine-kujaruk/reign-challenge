import { Injectable, HttpService } from '@nestjs/common';
import { Post } from './../../../dto/post.dto';
import { Hits } from './../../../dto/hits.dto';
import { URL_ALGOLIA_API } from './../../../config/index';

@Injectable()
export class ToolsService {
  constructor(private http: HttpService) {}

  formatPosts(hits: Hits[], lastPostCreated?: Date): Partial<Post>[] {
    if (!Array.isArray(hits)) return [];

    let response: Partial<Post>[] = hits
      .filter(({ title, story_title }) => title || story_title)
      .map(
        ({
          title,
          story_title,
          story_url,
          author,
          created_at,
        }): Partial<Post> => ({
          title: story_title || title,
          url: story_url,
          author,
          created_at,
        })
      );

    if (lastPostCreated) {
      response = response.filter(
        ({ created_at }) => new Date(created_at) > lastPostCreated
      );
    }

    return response.reverse();
  }

  async fetchPosts() {
    return this.http
      .get(`${URL_ALGOLIA_API}/search_by_date?query=nodejs`)
      .toPromise();
  }

  async getNewPosts(lastPostCreated?: Date): Promise<Partial<Post>[]> {
    try {
      const result = await this.fetchPosts();

      return this.formatPosts(result?.data?.hits || [], lastPostCreated);
    } catch (e) {
      return [];
    }
  }
}
