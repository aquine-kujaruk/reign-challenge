import Repository from './repository';

const initialData = {
  posts: [],
  sending: false,
  error: null,
  message: null,
};

const GET_POSTS_SENDING = 'GET_POSTS_SENDING';
const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
const GET_POSTS_FAILED = 'GET_POSTS_FAILED';
const DELETE_POSTS_SENDING = 'DELETE_POSTS_SENDING';
const DELETE_POSTS_SUCCESS = 'DELETE_POSTS_SUCCESS';
const DELETE_POSTS_FAILED = 'DELETE_POSTS_FAILED';

export default function reducer(state = initialData, { type, payload }) {
  switch (type) {
    case GET_POSTS_SENDING:
      return { ...state, sending: true, error: null, message: null };
    case GET_POSTS_SUCCESS:
      return {
        ...state,
        posts: payload.posts,
        sending: false,
        error: null,
        message: payload.message,
      };
    case GET_POSTS_FAILED:
      return { ...state, error: payload, sending: false };
    case DELETE_POSTS_SENDING:
      return { ...state, sending: true, error: null, message: null };
    case DELETE_POSTS_SUCCESS:
      return {
        ...state,
        posts: payload,
        sending: false,
        error: null,
        message: 'post deleted successfully',
      };
    case DELETE_POSTS_FAILED:
      return { ...state, error: payload, sending: false };
    default:
      return state;
  }
}

export const getPosts = () => async (dispatch, getState) => {
  dispatch({
    type: GET_POSTS_SENDING,
    payload: {},
  });
  try {
    const currentPosts = getState().data.posts;
    const id =
      (currentPosts.length > 0 && currentPosts[currentPosts.length - 1].id) ||
      null;

    const posts = await Repository.getPosts(id);
    const payload =
      posts.length > 0
        ? { posts: [...currentPosts, ...posts], message: null }
        : { posts: [...currentPosts], message: 'No more posts to load' };

    dispatch({
      type: GET_POSTS_SUCCESS,
      payload,
    });
  } catch (error) {
    dispatch({
      type: GET_POSTS_FAILED,
      payload: error.message,
    });
  }
};

export const deletePost = (id) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_POSTS_SENDING,
    payload: {},
  });
  try {
    const currentPosts = getState().data.posts;

    await Repository.deletePost(id);

    const posts = currentPosts.filter((post) => post.id !== id);

    dispatch({
      type: DELETE_POSTS_SUCCESS,
      payload: posts,
    });
  } catch (error) {
    dispatch({
      type: DELETE_POSTS_FAILED,
      payload: error.message,
    });
  }
};
