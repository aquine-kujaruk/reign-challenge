import {Module, HttpModule} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {PostController} from './post.controller';
import {PostRepository} from './post.repository';
import {PostSchema} from '../../schemas/post.schema';
import { ToolsService } from './tools/tools.service';

@Module({
	imports: [
		HttpModule,
		MongooseModule.forFeature([
			{
				name: 'Post',
				schema: PostSchema,
			},
		]),
	],
	controllers: [PostController],
	providers: [PostRepository, ToolsService],
	exports: [PostRepository],
})
export class PostModule {}
