import React, { useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { getPosts, deletePost } from '../duck';
import { formatDate } from '../utils';

const MyTable = () => {
  const dispatch = useDispatch();
  const alert = useAlert();
  const { posts, error, message } = useSelector((state) => state.data);

  useEffect(() => {
    dispatch(getPosts());
  }, [dispatch]);

  useEffect(() => {
    if (error) alert.error(error);
  }, [error, alert]);

  useEffect(() => {
    if (message) alert.success(message);
  }, [message, alert]);

  const openUrl = (url) => {
    if (url) window.open(url, '_blank');
    else alert.error('The website link is not valid');
  };

  const onDeletePost = (e, id) => {
    e.stopPropagation();
    dispatch(deletePost(id));
  };

  return (
    <div className="table-component">
      <div className="container">
        <table>
          <tbody>
            {posts.map((post) => (
              <tr key={post.id} onClick={() => openUrl(post.url)}>
                <th className="typography" role="cell" scope="row">
                  <div className="flex-container">
                    <p className="table-text text-dark">{post.title}</p>
                    <p className="table-text table-title text-light">
                      -{post.author}-
                    </p>
                  </div>
                </th>
                <th className="typography" role="cell" scope="row">
                  <p className="table-text text-dark">
                    {formatDate(post.created_at)}
                  </p>
                </th>
                <th className="typography align-right">
                  <button
                    className="button icon-button"
                    onClick={(e) => onDeletePost(e, post.id)}
                  >
                    <FontAwesomeIcon icon={faTrash} />
                  </button>
                </th>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="clickable-button-container">
        <button
          onClick={() => dispatch(getPosts())}
          className="clickable-button"
        >
          Load More
        </button>
      </div>
    </div>
  );
};

export default MyTable;
