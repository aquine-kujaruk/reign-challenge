class Repository {
  apiUrl: string;

  constructor() {
    this.apiUrl = 'http://localhost:5000/api';
  }

  async handleErrors(res, withReturn = true) {
    if (!res.ok) {
      const { message } = await res.json();
      throw new Error(message);
    }
    return withReturn && res.json();
  }

  payload(method: any, body = {}): any {
    const response = {
      method: method,
      mode: 'cors',
      cache: 'no-cache',
      headers: { 'Content-Type': 'application/json' },
    };

    if (method.toLowerCase() !== 'get') response['body'] = JSON.stringify(body);

    return response;
  }

  async getPosts(id) {
    const limit = 15;
    const lastPostViewed = (id && `&lastPostViewed=${id}`) || '';
    const url = `${this.apiUrl}/posts?limit=${limit}${lastPostViewed}`;

    const res = await fetch(url, this.payload('GET'));

    return this.handleErrors(res);
  }

  async deletePost(id) {
    const url = `${this.apiUrl}/posts/${id}`;
    const res = await fetch(url, this.payload('DELETE'));
    await this.handleErrors(res, false);
  }
}

export default new Repository();
