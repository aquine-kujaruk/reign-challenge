import {IsBoolean, IsDateString, IsString, IsUrl} from 'class-validator';
import {Document} from 'mongoose';

export class Post extends Document {
	@IsString()
	title: string;

	@IsUrl()
	url: string;

	@IsString()
	author: string;

	@IsDateString()
	created_at: Date | string;

	@IsBoolean()
	status: boolean;
}
