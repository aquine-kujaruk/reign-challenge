import { Controller, Delete, Get, Param, Query } from '@nestjs/common';
import { PostRepository } from './post.repository';
import { Post } from '../../dto/post.dto';
import { ToIntegerPipe } from './../../pipes/to-integer.pipe';

@Controller('posts')
export class PostController {
  constructor(private postRepository: PostRepository) {}

  @Get()
  getPosts(
    @Query('lastPostViewed') lastPostViewed: string,
    @Query('limit', ToIntegerPipe) limit: number
  ): Promise<Post[]> {
    return this.postRepository.getPosts(lastPostViewed, limit);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<void> {
    return this.postRepository.delete(id);
  }
}
