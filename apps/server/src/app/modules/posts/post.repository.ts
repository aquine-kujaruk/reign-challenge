import {
	Injectable,
	InternalServerErrorException,
	HttpService,
	NotFoundException,
} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Post} from './../../dto/post.dto';
import {ToolsService} from './tools/tools.service';

@Injectable()
export class PostRepository {
	constructor(
		@InjectModel('Post') private postDb: Model<Post>,
		private http: HttpService,
		private tools: ToolsService,
	) {
		this.saveNewPosts();
	}

	async getLastPostCreated(): Promise<Date> {
		const lastPost = await this.postDb
			.findOne()
			.sort({created_at: -1})
			.select('created_at');

		return lastPost?.created_at as Date;
	}

	insertMany(posts: Array<Partial<Post>>): Promise<Post[]> {
		return this.postDb.insertMany(posts);
	}

	async saveNewPosts(): Promise<void> {
		const lastPostCreated: Date = await this.getLastPostCreated();

		const posts: Partial<Post>[] = await this.tools.getNewPosts(
			lastPostCreated,
		);

		await this.insertMany(posts);
	}

	async getPosts(lastPostViewed: string, limit: number): Promise<Post[]> {
		const query = (lastPostViewed && {_id: {$lt: lastPostViewed}}) || {};
		try {
			return this.postDb
				.find({...query, status: true})
				.sort({created_at: -1})
				.limit(limit);
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	async delete(id: string): Promise<void> {
		try {
			const document = await this.postDb.findOneAndUpdate(
				{_id: id},
				{status: false},
			);

			if (!document)
				throw new NotFoundException(
					`Could not find post to delete for id: ${id}`,
				);
		} catch (e) {
			if (e.status === 404) throw e;
			throw new InternalServerErrorException(e);
		}
	}
}
