import {Injectable, Logger} from '@nestjs/common';
import {Cron, CronExpression} from '@nestjs/schedule';
import {PostRepository} from './../modules/posts/post.repository';

@Injectable()
export class ScheduleService {
	private readonly logger = new Logger(ScheduleService.name);

	constructor(private postRepository: PostRepository) {}

	@Cron(CronExpression.EVERY_HOUR)
	handleCron() {
		this.postRepository.saveNewPosts();
	}
}
